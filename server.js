const express = require('express');
const app = express();
const http = require('http').Server(app);
const io = require('socket.io')(http);
const fs = require('fs');
const os = require('os');
const script = require("./routes/funcAll");
const start = require("./routes/index.js");
const exec = require('executive');

global.lastRun = false;

app.set("view engine", "ejs");
app.use(express.static('views'));

app.get('/', function (req, res) {
    let logFile = 'clear';
    let option = script.parsingOption('option.json');

    res.render('index', {
        option,
        logFile,
        lastRun: global.lastRun,
    });

});

app.get('/performance.log', function (req, res) {
    let file = __dirname + '/performance.log';
    res.download(file);
});

app.get('/accs.json', function (req, res) {
    let file = __dirname + '/accs.json';
    res.download(file);
});


setInterval(function () {
    let proces = os.loadavg();
    console.log('proc:', proces);
    if(proces[2] < 0.04 ){
        exec('pm2 restart 0');
    }


}, 900000);


global.lastRun = 0;
setInterval(async function () {
    if (global.lastRun <= 2) {
        let objApi = await JSON.parse(await script.apiRequestIn());
        console.log(objApi);
        if (typeof objApi.setting === 'object' && objApi.setting !== null) {
            await start.Index(objApi);
        }

    }
}, 20000);


io.on('connection', function (socket) {
    console.log('connect!');


    socket.on('disconnect', function () {
        console.log('disconnect!');
    });

    socket.on('tryFormSend', function (req) {

        global.doneCount = 0;

        setTimeout(function () {
            script.runCount();
        }, 500);

    });

    socket.on('optionFormSend', function (req) {
        let option = {
            "global": {
                "timeoutAll": req[0],
                "timeoutImportant": req[1],
                "timeoutWaitSms": req[2],
                "tryTime": req[3],
                "proxy": req[4],
                "proxyCount": req[5],
                "userAgent": req[6],
                "userAgentCount": req[7],
                "firstName": req[8],
                "firstNameCount": req[9],
                "lastName": req[10],
                "lastNameCount": req[11]
            }
        };
        script.saveOption(option);
    });
    try {

        fs.watchFile('./performance.log', (curr, prev) => {
            let logFile = fs.readFileSync('./performance.log', 'utf-8');
            io.emit('logs', logFile);
        });
    } catch (e) {

    }

    io.emit('RunFunc', global.lastRun);

});


http.listen(3000, function () {
    console.log(`app listening on port 3000!`);
});

