function Index(infObj) {
    const puppeteer = require("puppeteer-extra");
    const pluginStealth = require("puppeteer-extra-plugin-stealth");
    const fs = require("fs");
    const log4js = require('log4js');
    let script = require("./funcAll.js");

    puppeteer.use(pluginStealth());

    global.lastRun = global.lastRun + 1;

    log4js.configure({
        appenders: {result: {type: 'file', filename: 'performance.log'}},
        categories: {default: {appenders: ['result'], level: 'info'}}
    });
    const logger = log4js.getLogger('result');

    if (typeof infObj.setting.surname !== "string" || typeof infObj.setting.surname !== "string") {
        global.lastRun = global.lastRun - 1;
        script.apiRequestOut('error', 'invalidType surname or name');
        return;
    }

    const timeoutAll = infObj.setting.time_delay * 1000;
    const timeoutImportant = infObj.setting.important_delay * 1000;
    const timeoutWaitSms = infObj.setting.time_expectations * 1000;
    let proxy = infObj.setting.proxy;
    let userAgent = infObj.setting.useragent[0];
    console.log(userAgent[0]);
    let firstName = infObj.setting.name;
    let lastName = infObj.setting.surname;


////////////////////////////////////
    return (async () => {

        console.log(proxy);

        const browser = await puppeteer.launch({
            headless: true,
            args: [
                '--no-sandbox',
                '--proxy-server=' + proxy
            ]
        });
        //новая вкладка
        const page = await browser.newPage();

        //переходим по адресу
        await page.emulate({
            viewport: {
                width: 720,
                height: 1480,
                isMobile: true,
                hasTouch: true
            },
            userAgent: userAgent
        });

        let ress = await script.getPhoneNumber(infObj);

        let price = ress[0];
        let resValid = ress[1];
        console.log('ress: ', ress);
        console.log('price:', price);
        console.log('resValid:', resValid);
        if (!resValid) {
            setTimeout(async function () {
                await page.deleteCookie();
                global.lastRun = await global.lastRun - 1;
                await browser.close()
            }, 1000);
            return;
        }
        await page.waitFor(timeoutAll);

        let number = resValid[2];
        await script.apiRequestOut('info', 'Получаем номер: ' + number + ' по цене: ' + price);
        let id = resValid[1];


        try {
            await page.goto('https://m.vk.com/join', {waitUntil: 'domcontentloaded'});
            await page.waitFor(timeoutAll);
            await logger.info('==========Начало попытки==========');
            await script.apiRequestOut('info', 'Начало попытки');
            await page.waitFor(timeoutAll);
            await page.focus('#mcont > div > div > div > form > div:nth-child(3) > input');

        } catch (e) {
            // socket.emit('RunFunc', true);

            await script.apiRequestOut('info', 'Начало попытки');
            await logger.info('==========Начало попытки==========');
            await script.apiRequestOut('error', 'Битый прокси!');
            await logger.error('Битый прокси!');
            await logger.info('==========Конец попытки==========');
            await script.apiRequestOut('info', 'Конец попытки');
            global.try = true;
            await script.doRequest('https://smshub.org/stubs/handler_api.php?api_key=' + infObj.setting.key_sms + '&action=setStatus&status=8&id=' + id);

            await page.deleteCookie();
            await page.screenshot({path: 'Three.png'});
            global.lastRun = await global.lastRun - 1;
            setTimeout(async () => await browser.close(), 1000);
            return;
        }


        try {
            await page.keyboard.type(firstName);
            await page.waitFor(timeoutAll);
            // фамилия
            await page.focus('#mcont > div > div > div > form > div:nth-child(4) > input');
            await page.keyboard.type(lastName);
            await page.waitFor(timeoutAll);
            // день рождения
            let dateArr = await infObj.setting.birthday.split('-');
            dateArr[1] = await dateArr[1].charAt(0) !== '0' ? dateArr[1] : dateArr[1] = dateArr[1].substr(1);
            dateArr[2] = await dateArr[2].charAt(0) !== '0' ? dateArr[2] : dateArr[2] = dateArr[2].substr(1);
            await page.select('#mcont > div > div > div > form > dl > dd > table > tbody > tr > td:nth-child(1) > select', dateArr[2]);
            await page.waitFor(timeoutAll);
            // месяц рождения
            await page.select('#mcont > div > div > div > form > dl > dd > table > tbody > tr > td:nth-child(2) > select', dateArr[1]);
            await page.waitFor(timeoutAll);
            // год рождения
            await page.select('#mcont > div > div > div > form > dl > dd > table > tbody > tr > td.row_table_last_column > select', dateArr[0]);
            await page.waitFor(timeoutAll);
            // жмём отправить
            await page.click('#mcont > div > div > div > form > div.fi_row_new > input');

        } catch (e) {
            await script.apiRequestOut('info', 'Битый прокси');
            await logger.info('==========Конец попытки==========');
            await script.doRequest('https://smshub.org/stubs/handler_api.php?api_key=' + infObj.setting.key_sms + '&action=setStatus&status=8&id=' + id);

            setTimeout(async function () {
                await page.deleteCookie();
                global.lastRun = await global.lastRun - 1;
                await browser.close()
            }, 1000);
            return;
        }

        try {
            await page.waitFor(timeoutAll);
            // обязательно ждём две секунды(1 мало) что бы появился выбор пола
            await page.waitFor(timeoutImportant);
            // выбираем мужской
            await page.click('#mcont > div > div > div > form > dl:nth-child(6) > dd > label:nth-child(' + (infObj.setting.gender === 0 ? 2 : 1) + ')');
            // жмём отправить
            await page.waitFor(timeoutAll);
            await page.click('#mcont > div > div > div > form > div.fi_row_new > input');

        } catch (e) {

        }
        try {
            await page.waitFor(timeoutAll);
            // страна
            await page.select('#mcont > div > div > div > form > dl:nth-child(3) > dd > div > select', '+7');
            await page.waitFor(timeoutAll);
            // номер
            await page.focus('#mcont > div > div > div > form > dl:nth-child(4) > dd > label > div.TextInput__nativeWrap > input');
        } catch (e) {
            await script.apiRequestOut('info', 'Битый прокси');
            await logger.info('==========Конец попытки==========');
            setTimeout(async function () {
                // socket.emit('RunFunc', false);
                await script.doRequest('https://smshub.org/stubs/handler_api.php?api_key=' + infObj.setting.key_sms + '&action=setStatus&status=8&id=' + id);

                await page.deleteCookie();
                global.lastRun = await global.lastRun - 1;
                await browser.close()
            }, 1000);
            return;
        }


        // вводим его в поле
        await page.keyboard.type(await number.slice(1));
        await page.waitFor(timeoutAll);
        // жмём на чекбокс соглашаясь
        await page.click('#mcont > div > div > div > form > dl:nth-child(5) > dd > label.Join__accept.Join__accept_eu_no.Control.Control_type_checkbox.Row > div.Control__label');
        await page.waitFor(timeoutAll);


        // идём на получить код
        await page.setJavaScriptEnabled(false);
        await page.click('#mcont > div > div > div > form > div > button');
        await page.waitFor(timeoutAll);
        try {
            await page.click('#mcont > div > div > div > form > div > button');
        } catch (e) {

        }

        await page.waitFor(timeoutAll);
        // Ловим ошибку которую выкидывает кукловод если не находит текст инвалидности номера.
        try {
            let warnVk = await page.evaluate(() => document.querySelector('#mcont > div > div > div > div > div > div').innerText);
            await page.waitFor(timeoutAll);
            if (warnVk) {
                await script.apiRequestOut('error', 'Вк выбросил ошибку: ' + warnVk);
                await logger.error('Вк выбросил ошибку: ' + warnVk);
                await script.doRequest('https://smshub.org/stubs/handler_api.php?api_key=' + infObj.setting.key_sms + '&action=setStatus&status=8&id=' + id);
                await script.apiRequestOut('info', 'Отменяю активацию номера!');
                await script.apiRequestOut('info', 'Конец попытки');
                await logger.info('Отменяю активацию номера!');
                await logger.info('==========Конец попытки==========');
                setTimeout(async function () {
                    await page.deleteCookie();
                    global.lastRun = await global.lastRun - 1;
                    await browser.close();
                }, 1000);
                return;
            }
        } catch (e) {
            await logger.info('Номер валидный!');
            await script.apiRequestOut('info', 'Номер валидный!');
        }
        await logger.info('Вк отправляет смс');
        await script.apiRequestOut('info', 'Вк отправляет смс');

        // ждём смс
        await page.waitFor(timeoutWaitSms);

        // запрашиваем статус смс

        let res2 = await script.doRequest('https://smshub.org/stubs/handler_api.php?api_key=' + infObj.setting.key_sms + '&action=getStatus&id=' + id);

        await page.waitFor(timeoutAll);

        let res2Valid = await script.checkResponse(res2);

        await page.waitFor(timeoutAll);

        if (!res2Valid) {

            await logger.info('Жду ' + timeoutWaitSms / 1000 + 'с что бы снова запросить смс');
            await script.apiRequestOut('info', 'Жду ' + timeoutWaitSms / 1000 + 'с что бы снова запросить смс');

            try {
                await page.click('#mcont > div > div > div > form > div.fi_row.wide_button > div > a');
            } catch (e) {

            }

            await page.waitFor(timeoutWaitSms);

            res2 = await script.doRequest('https://smshub.org/stubs/handler_api.php?api_key=' + infObj.setting.key_sms + '&action=getStatus&id=' + id);

            await page.waitFor(timeoutAll);

            res2Valid = await script.checkResponse(res2);

            await page.waitFor(timeoutAll);

            if (!res2Valid) {
                await script.doRequest('https://smshub.org/stubs/handler_api.php?api_key=' + infObj.setting.key_sms + '&action=setStatus&status=8&id=' + id);
                await script.apiRequestOut('info', 'Отменяю активацию номера!');
                await script.apiRequestOut('info', 'Конец попытки');
                await logger.info('Отменяю активацию номера!');
                await logger.info('==========Конец попытки==========');
                setTimeout(async function () {
                    // socket.emit('RunFunc', false);

                    await page.deleteCookie();
                    global.lastRun = await global.lastRun - 1;
                    await browser.close()
                }, 1000);
                return;
            }
        }


        await page.waitFor(timeoutAll);

        let codeSms = res2Valid[1];

        // вписываем код и жмём продолжить
        await page.focus('#mcont > div > div > div > form > dl:nth-child(4) > dd > input');
        await page.waitFor(timeoutAll);

        await page.keyboard.type(codeSms);
        await page.waitFor(timeoutAll);
        // await page.waitFor(timeoutAll);

        await page.click('#mcont > div > div > div > form > div.fi_row_new > input');
        await page.waitFor(timeoutAll);

        // проверяем принял ли вк код
        let k = 0;
        while (k < 5) {
            await k++;
            // await page.waitFor(timeoutAll);
            try {
                // генерируем пароль и вводим его в поле, после чего нажимаем регистрация
                await page.waitFor(timeoutAll);
                await page.waitFor(timeoutAll);
                await page.focus('#mcont > div > div > div > form > dl:nth-child(5) > dd > input');
                await page.waitFor(timeoutAll);
                // await page.evaluate(() => document.querySelector('#mcont > div > div > div > form > dl:nth-child(5) > dt').innerText);

                await logger.info('Вк успешно принял код!');
                await script.apiRequestOut('info', 'Вк успешно принял код!');
                k = 5;
            } catch (e) {
                try {
                    console.log('что-то не так');
                    await page.screenshot({path: 'scr1.png'});
                    await page.goBack();

                    await page.waitFor(timeoutAll);
                    await page.waitFor(timeoutAll);
                    await page.screenshot({path: 'scr2.png'});
                    await page.click('#mcont > div > div > div > form > div.fi_row_new > input');
                } catch (e) {

                }
            }


            if (k > 5) {
                await script.apiRequestOut('error', 'Что-то пошло не так, детальней в скриншоте');
                await script.apiRequestOut('info', 'Конец попытки');
                await script.apiRequestPay(infObj.setting.id, price);
                await logger.info('Что-то пошло не так, детальней в скриншоте');
                await logger.info('==========Конец попытки==========');
                await page.screenshot({path: 'scr.png'});
                setTimeout(async function () {
                    // socket.emit('RunFunc', false);

                    await page.deleteCookie();
                    global.lastRun = await global.lastRun - 1;
                    await browser.close()
                }, 1000);
                return;
            }

        }


        let password = await script.generatePassword();

        await script.apiRequestOut('info', 'Сгенерирован пароль: ' + password);
        await logger.info('Сгенерирован пароль: ' + password);

        await page.keyboard.type(password);
        await page.waitFor(timeoutAll);
        // await page.waitFor(timeoutAll);

        await page.click('#mcont > div > div > div > form > div > input');

        await page.waitFor(timeoutImportant);

        // проверяем зарегистрировались ли и записываем данные
        let j = 0;
        while (j < 5) {
            await j++;
            // await page.waitFor(timeoutAll);
            try {
                await page.waitForSelector('#mcont > div > div > div > div > div > div', {timeout: 8000});
                await page.click('#mcont > div > div > div > form > div:nth-child(3) > a');
                await page.waitFor(timeoutAll);
                await page.click('#sf_search_items > div:nth-child(2) > a');
                await page.waitFor(timeoutAll);
                await page.click('#mcont > div > div > div > form > div:nth-child(4) > a');
                await page.waitFor(timeoutAll);
                await page.click('#sf_search_items > div:nth-child(2)');
                await page.waitFor(timeoutAll);
                await page.click('#mcont > div > div > div > form > div.fi_row_new > input');
                j = 6;

            } catch (e) {
                console.log('not found selector');
                await page.waitFor(timeoutAll);
                await page.waitFor(timeoutAll);

                try {
                    await page.goBack();

                    await page.waitFor(timeoutAll);
                    await page.waitFor(timeoutAll);
                    await page.focus('#mcont > div > div > div > form > dl:nth-child(5) > dd > input');
                    await page.keyboard.type(password);
                    await page.waitFor(timeoutAll);
                    await page.click('#mcont > div > div > div > form > div > input');
                } catch (e) {

                }

                if (j > 5) {
                    await script.apiRequestOut('error', 'Что-то пошло не так, детальней в скриншоте Three.png');
                    await script.apiRequestPay(infObj.setting.id, price);
                    await script.apiRequestOut('info', 'Конец попытки');
                    await logger.info('Что-то пошло не так, детальней в скриншоте Three.png');
                    await logger.info('==========Конец попытки==========');
                    await page.screenshot({path: 'Three.png'});
                    setTimeout(async function () {
                        await page.deleteCookie();
                        global.lastRun = await global.lastRun - 1;
                        await browser.close()
                    }, 1000);
                    return;
                }

            }

        }

        try {
            await script.apiRequestOut('info', 'Регистрация прошла успешно!');
            await logger.info('Регистрация прошла успешно!');


            global.try = true;
            global.success = true;

            let acc = {
                number: number,
                password: password,
                userAgent: userAgent,
                proxy: proxy,
                firstName: firstName,
                lastName: lastName
            };

            let dataOld = await fs.readFileSync(__dirname + '/../accs.json', 'utf8');

            await script.readFileCallback(dataOld, acc);

        } catch (e) {
            await script.apiRequestOut('info', 'Сохраненить данные аккаунта неудалось!');
            await logger.error('Сохраненить данные аккаунта неудалось!');
        }

        await page.waitFor(timeoutAll);
        try {
            await page.click('#mcont > div > div > div > form > div:nth-child(11) > div > a');
            await page.waitFor(timeoutAll);
        } catch (e) {

        }

        // жмём пропустить доп инфу
        try {
            await page.click('#mcont > div > div > div > form > div:nth-child(11) > div > a');
        } catch (e) {

        }

        await page.waitFor(timeoutImportant);

        // жмём пропустить ввод емейла
        try {
            await page.click('#mcont > div > div > div > form > div:nth-child(5) > div > a');
            await page.waitFor(timeoutAll);

            await page.waitFor(timeoutAll);
            await page.click('#mcont > div.pcont > div > div.wall_item.feedAssistance.feedAssistance_profilePhoto > div.feedAssistance__footer > div > label > a');
            await page.waitFor(timeoutAll);

        } catch {

        }

        try {
            // пробуем добавить аву
            if ((await script.addAvu(page, infObj, timeoutAll)) !== true) {
                await console.log('false ava 1');
            }
        } catch (e) {

        }

        try {
            // добавляем фото на стену
            if ((await script.addPhoto(page, infObj, timeoutAll)) !== true) {
                await console.log('false foto');
            }
        } catch (e) {

        }
        await page.waitFor(timeoutAll);
        try {
            // добавляем статус
            if ((await script.addStatus(page, infObj, timeoutAll)) !== true) {
                await console.log('false status');
            }
            await page.waitFor(timeoutAll);
        } catch (e) {

        }

        await page.waitFor(timeoutAll);

        // сохраняем ссылку на страницу
        try {
            await page.click('#lm_prof_panel > div')
        } catch (e) {

        }
        // page.click('#lm_prof_panel > div').then({}).catch({});
        let checkUrl = await page.evaluate(() => location.href);


        try {
            // пробуем вступать в группы
            if ((await script.addEntry(page, infObj, timeoutAll)) !== true) {
                await console.log('false groups');
            }
        } catch (e) {

        }

        await page.waitFor(timeoutAll);
        try {
            // пробуем репостить
            if ((await script.addRepost(page, infObj, timeoutAll)) !== true) {
                await console.log('false repost');
            }
        } catch (e) {

        }

        try {
            // страховочная попытка добавить аву
            await page.waitFor(timeoutAll);
            await page.goto('https://m.vk.com/feed', {waitUntil: 'domcontentloaded'});
            if ((await script.addAvu(page, infObj, timeoutAll)) !== true) {
                await console.log('false ava 2');
            }
        } catch (e) {

        }


        // собираем все данные для передачи по апи авторега
        await console.log('id', infObj.setting.id);
        await console.log('number', number);
        await console.log('password', password);
        await console.log('checkUrl', checkUrl);
        await console.log('gender', infObj.setting.gender);


        // передаём авторег
        await script.apiRequestAutoregs(infObj.setting.id, number, password, checkUrl, infObj.setting.gender, price);
        await script.apiRequestOut('info', 'Данные авторега отправленны');

        await script.apiRequestOut('info', 'Конец попытки');
        await logger.info('==========Конец попытки==========');
        setTimeout(async function () {
            await page.deleteCookie();
            global.lastRun = await global.lastRun - 1;
            await browser.close()
        }, 1000);
        return true;
    })();


}


module.exports = {
    Index: Index

};



