const request = require("request");
const fs = require("fs");
const log4js = require('log4js');
const script = require("./index.js");
const date = require('date-and-time');
const download = require('download');


log4js.configure({
    appenders: {result: {type: 'file', filename: 'performance.log'}},
    categories: {default: {appenders: ['result'], level: 'info'}}
});
const logger = log4js.getLogger('result');

let obj = {
    accs: []
};
let json = JSON.stringify(obj);

function doRequest(url) {
    return new Promise(function (resolve, reject) {
        request(url, function (error, res, body) {
            if (!error && res.statusCode == 200) {
                console.log(body);
                resolve(body);
            } else {
                reject(error);
            }
        });
    });
}


// Функция проверки ответа
function checkResponse(response) {

    if (response.toString().indexOf(':') > -1) {
        response = response.split(':');
        logger.info('Ответ: ' + response);
    }


    if (!response || (response[0] !== 'ACCESS_NUMBER') && (response[0] !== 'STATUS_OK') && (response[0] !== 'ACCESS_BALANCE')) {
        logger.error('Что-то не так: ' + response);
        apiRequestOut('info', response);
        setTimeout(function () {
            return false;
        }, 1000);
    } else {
        logger.info('Ответ успешно прошёл проверку!');
        return response;
    }


}


function generatePassword() {
    let length = 8,
        charset = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789",
        retVal = "";
    for (let i = 0, n = charset.length; i < length; ++i) {
        retVal += charset.charAt(Math.floor(Math.random() * n));
    }
    return retVal;
}

function readFileCallback(dataOld, acc) {

    obj = JSON.parse(dataOld); //now it an object

    obj.accs.push({acc}); //add some data

    json = JSON.stringify(obj); //convert it back to json

    fs.writeFile(__dirname + '/../accs.json', json, 'utf8', callback); // write it back
}


function randomInteger(min, max) {
    let rand = min - 0.5 + Math.random() * (max - min + 1);
    return Math.round(rand);
}


function get_line(filename, line_no, callback) {
    line_no = parseInt(line_no);
    let data = fs.readFileSync(filename, 'utf8');
    let lines = data.split("\n");
    for (let l in lines) {
        if (l == line_no - 1) {
            callback(null, lines[l].trim());
            return;
        }
    }
    throw new Error('File end reached without finding line');
}

function runCount() {

    if (global.lastRun >= 1) {
        console.log('Already run!');
    } else {
        // global.lastRun = global.lastRun + 1;
        try {
            let fname = date.format(new Date(), 'YYYY-MM-DD_HH-mm-ss');
            let i = 1;

            logStorageSend(fname);

            let timerId = setTimeout(async function tick() {
                if (i >= Number(global.countReg)) {
                    clearTimeout(timerId);
                }

                global.success = false;

                let infObj = await apiRequestIn();

                infObj = JSON.parse(infObj);

                global.countReg = Number(infObj.setting.count_autoreg);

                await console.log(infObj);

                await script.Index(infObj);

                if (global.success === true) {
                    await i++;
                    console.log('global.success ===', i);
                }

                if (i < Number(global.countReg) + 1) {

                    await setTimeout(async function () {
                        await setTimeout(tick, 3000);
                    }, 3000);

                } else {
                    await clearTimeout(timerId);

                    global.lastRun = global.lastRun - 1;
                }
            }, 2000);

        } catch (err) {
            console.log('Already run!');

        }
    }
}


function proxyControll() {

    let i = 0;
    let proxyArr = [];


    const option = parsingOption('option.json');


    return parsingOption(option.global.proxy);
    // console.log(proxyArr[0][0]);

}


function logStorageSend(fname) {
    fs.openSync(__dirname + '/../logStorage/' + fname + '.log', 'w+');

    fs.writeFileSync(__dirname + '/../logStorage/' + fname + '.log', fs.readFileSync(__dirname + '/../performance.log', 'utf-8'));

    fs.writeFileSync(__dirname + '/../performance.log', '', 'utf-8');
}


function parsingOption(path) {
    return JSON.parse(fs.readFileSync(path));
}

function saveOption(optionObj) {
    json = JSON.stringify(optionObj);

    fs.writeFileSync('./option.json', json, callback());

    // console.log(json);
}

function apiRequestIn() {

    return new Promise(function (resolve, reject) {
        request('http://avtoreg.teo-crm.ru/api/list/task', function (error, res, body) {
            if (!error && res.statusCode === 200) {
                // console.log(body);
                resolve(body);
            } else {
                reject(error);
            }
        });
    });
}

function apiRequestOut(type, comment) {

    request.post('http://avtoreg.teo-crm.ru/api/list/logs').form({type: type, comment: comment})

}

function apiRequestPay(task_id, price) {

    request.post('http://avtoreg.teo-crm.ru/api/list/pay').form({task_id: task_id, price: price})

}

function apiRequestAutoregs(task_id, login, password, link, gender, price) {

    request.post('http://avtoreg.teo-crm.ru/api/list/auto-regs').form({
        "task_id": task_id,//number
        "login": login,//string
        "password": password,//string
        "link": link,//string
        "status": 1,//number
        "gender": gender,//number
        "add_information": 'some information',//string
        "proxy_type": 0, //number
        "price": price,
        "comment": 'some comment', //string
    })

}

async function addAvu(page, infObj, timeoutAll) {

    // закидаем аватарку


    await page.waitFor(timeoutAll);
    await page.waitFor(timeoutAll);

    console.log('foto start');

    try {
        await page.click('#mcont > div.pcont > div > div.wall_item.feedAssistance.feedAssistance_profilePhoto > div.feedAssistance__footer > div > label > a');
    } catch (e) {
        console.log('no button add photo');
    }
    await page.waitFor(timeoutAll);
    try {
        let UploadElement = await page.$('#mcont > div > div > div > form > div:nth-child(1) > input');

        await page.waitFor(timeoutAll);

        let avaArr = await infObj.setting.photo[0].split('/');

        await download(infObj.setting.photo[0], __dirname + '/../tempPhoto/').then(() => {
            // console.log('done!');
        });
        await page.waitFor(timeoutAll);
        await page.waitFor(timeoutAll);

        await UploadElement.uploadFile(__dirname + '/../tempPhoto/' + avaArr[avaArr.length - 1]);
    } catch (e) {
        console.log('not input upload');
    }


    await page.waitFor(timeoutAll);

    try {
        await page.click('#mcont > div > div > div > form > div:nth-child(2) > input');
    } catch (e) {

    }


    await page.waitFor(3000);
}

async function addPhoto(page, infObj, timeoutAll) {

    // начинаем добавлять по 1 фото на стену
    let i = 0;
    while (await infObj.setting.photo.length - 1 > i) {
        await i++;
        await page.waitFor(timeoutAll);
        // качаем первое фото
        console.log("Downloading photo");
        console.log(infObj.setting.photo[i]);
        console.log(__dirname + '/../tempPhoto/');
        try {
            await download(infObj.setting.photo[i], __dirname + '/../tempPhoto/').then(() => {
                console.log('done downloading photo');
            });
        } catch (e) {

        }

        await page.waitFor(timeoutAll);

        //переходим на страницу добавления записи
        try {
            await page.goto('https://m.vk.com/wall?act=add&from=profile', {waitUntil: 'domcontentloaded'});

            await page.waitFor(timeoutAll);

            //вводим коментарий
            await page.focus('#feed_add_form > div.MentionContainer > div.iwrap > textarea');
            await page.waitFor(1000);
            await page.keyboard.type('😉');
            await page.waitFor(1000);
            //нажимаем на прикрепить файл
            await page.click('#attach_photo_btn > input');
        } catch (e) {

        }


        await page.waitFor(timeoutAll);

        try {
            //находим инпут файл
            let UploadElement1 = await page.$('#mcont > div > div > div > form > div:nth-child(1) > input');

            await page.waitFor(timeoutAll);

            //берём имя файла и выгружаем его в инпут файл
            let imgArr = await infObj.setting.photo[i].split('/');

            await UploadElement1.uploadFile(
                __dirname + '/../tempPhoto/' + imgArr[imgArr.length - 1]
            );

            await console.log('uploadd sucss');

            await page.waitFor(timeoutAll);

            //жмём загрузить
            await page.click('#mcont > div > div > div > form > div:nth-child(4) > input');

        } catch (e) {

        }

        try {
            await page.waitForNavigation();
        } catch (e) {

        }

        await page.waitFor(timeoutAll);

        await console.log('push continue');

        //нажимаем продолжить
        try {
            await page.click('#feed_add_form > div.ibwrap > div.cp_buttons_block > button');
        } catch (e) {

        }
        await page.waitFor(timeoutAll);
        await console.log('push continue2');
    }

    // console.log('asolve');
    //
    // await page.waitFor(timeoutAll);
    // const directory = __dirname + '/../tempPhoto';
    // console.log('asolve2');

    // try {
    //     await rmDir(directory, false);
    // } catch (e) {
    //     console.log('delete no succes');
    // }
    // try {
    //     fs.readdir(directory, (err, files) => {
    //         if (err) throw err;
    //
    //         for (const file of files) {
    //             fs.unlink(path.join(directory, file), err => {
    //                 if (err) throw err;
    //             });
    //         }
    //     });
    // }catch (e) {
    //     console.log('delete no succes');
    // }
    await console.log('push continue3');
    return true;

}

async function addStatus(page, infObj, timeoutAll) {
    await console.log('status start1');
    try {
        await page.goto('https://m.vk.com/menu', {waitUntil: 'domcontentloaded'});
    } catch (e) {

    }


    await console.log('status start2');

    await page.waitFor(timeoutAll);

    try {
        await page.click('#mcont > div.MainMenu > div > div.basisDefault__row.UserProfile.Pad > a > div.OwnerRow.OwnerRow_noPadding > div > div.OwnerRow__right > div.OwnerRow__header');

    } catch (e) {
        await console.log('1');
    }

    try {

        let checkUrl = await page.evaluate(() => location.href);
        await console.log('url:', checkUrl);

        await page.waitFor(timeoutAll);

        await page.click('#mcont > div > div > div.PageBlock > div.owner_panel.profile_panel > div > div.pp_no_status > a');

        await page.waitFor(timeoutAll);
    } catch (e) {
        return false;
    }

    try {
        await page.focus('#mcont > div > div > div > div.pp_edit_status > form > div.iwrap > input');

        await page.keyboard.type(infObj.setting.status);

        await page.waitFor(1000);

        await page.click('#mcont > div > div > div > div.pp_edit_status > form > div.pp_buttons_block > input');

        await page.waitFor(timeoutAll);
    } catch (e) {
        return false;
    }

    return true;
}


async function addEntry(page, infObj, timeoutAll) {

    let i = -1;
    while (await infObj.setting.entry.length > i) {

        await i++;
        try {
            await page.goto(infObj.setting.entry[i]);
            await console.log('entry:', infObj.setting.entry[i]);
            await page.waitFor(timeoutAll);
            // await page.waitFor(30000);

            if (await page.$('#mcont > div > div.PageBlock.PageBlock_row > div.basisGroup__main.owner_panel.profile_panel > div.basisGroup__buttonsRow > div > div.BtnStack__cell.BtnStack__cell_icon > a') !== null) {
                await page.click('#mcont > div > div.PageBlock.PageBlock_row > div.basisGroup__main.owner_panel.profile_panel > div.basisGroup__buttonsRow > div > div.BtnStack__cell.BtnStack__cell_icon > a')
            } else {
                await page.click('#mcont > div > div.PageBlock.PageBlock_row > div.basisGroup__main.owner_panel.profile_panel > div.basisGroup__buttonsRow > div');

            }


            await page.waitFor(timeoutAll);
        } catch (e) {

        }

    }
    return true;


}

async function addRepost(page, infObj, timeoutAll) {

    let i = -1;
    while (await infObj.setting.repost.length > i) {
        await i++;

        try {
            await page.goto(infObj.setting.repost[i]);

            await page.waitFor(timeoutAll);

            await page.click('.i_share');

            await page.waitFor(timeoutAll);

            await page.click('#publish_send_block > button');

            await page.waitFor(timeoutAll);

        } catch (e) {
// ++++++++++ щётчик
        }

    }

    return true;


}

async function getPhoneNumber(infObj) {
    let arrRes = [];
    // баланс до покупки
    let getBalanceBefore = await doRequest('https://smshub.org/stubs/handler_api.php?api_key=' + infObj.setting.key_sms + '&action=getBalance');
    let getBalanceBeforeValid = await checkResponse(getBalanceBefore);

    // получаем номер
    let res = await doRequest('https://smshub.org/stubs/handler_api.php?api_key=' + infObj.setting.key_sms + '&action=getNumber&service=vk&operator=any&country=0');

    console.log('Number: ', res);

    //баланс после покупки
    let getBalanceAfter = await doRequest('https://smshub.org/stubs/handler_api.php?api_key=' + infObj.setting.key_sms + '&action=getBalance');
    let getBalanceAfterValid = await checkResponse(getBalanceAfter);
    let price = await (Number(getBalanceBeforeValid[1]) - Number(getBalanceAfterValid[1]));
    await arrRes.push(price);
    await console.log('price:', price);

    let ress = await checkResponse(res);

    await console.log('Number2: ', ress);

    await arrRes.push(ress);
    return arrRes;
}


rmDir = function (dirPath, removeSelf) {
    if (removeSelf === undefined)
        removeSelf = true;

    let files = fs.readdirSync(dirPath);

    if (files.length > 0)
        for (let i = 0; i < files.length; i++) {
            let filePath = dirPath + '/' + files[i];
            if (fs.statSync(filePath).isFile())
                fs.unlinkSync(filePath);
            else
                rmDir(filePath);
        }
    if (removeSelf)
        fs.rmdirSync(dirPath);
};

function Exit() {
    process.exit(-1);
}

function callback(err) {
    console.log(err);
}


module.exports = {
    doRequest: doRequest,
    checkResponse: checkResponse,
    generatePassword: generatePassword,
    readFileCallback: readFileCallback,
    randomInteger: randomInteger,
    get_line: get_line,
    Exit: Exit,
    parsingOption: parsingOption,
    runCount: runCount,
    proxyControll: proxyControll,
    saveOption: saveOption,
    apiRequestIn: apiRequestIn,
    apiRequestPay: apiRequestPay,
    apiRequestOut: apiRequestOut,
    apiRequestAutoregs: apiRequestAutoregs,
    addAvu: addAvu,
    addPhoto: addPhoto,
    addStatus: addStatus,
    addEntry: addEntry,
    addRepost: addRepost,
    getPhoneNumber: getPhoneNumber,
    callback: callback
};


